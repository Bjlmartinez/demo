import React from 'react';


class DataUser extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loadData: false,
      userdata: []
    }
  }

  getData(token) {
    return fetch('http://localhost:3001/personalData', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'access-token': token
      }
    })
      .then(data => {
        if (!data.ok) throw new Error('Response is NOT ok')
        return data.json()
      })
      .then((dataResponse) => {
        console.log("Respuesta", dataResponse);
        this.setState({ loadData: true, userdata: dataResponse })
      })
      .catch(console.log)
  }

  componentDidMount() {
    this.getData(JSON.parse(sessionStorage.getItem('token')).token);
  }

  render() {
    const { loadData, userdata } = this.state

    if (!loadData) { return <div>Cargando ...</div> }
    return (

    // <div className="book-details">
    //   <div>Datos personales </div>
    //   {
    //     userdata.map(
    //       (data)=>(
    //       <div>ID customer: {data.id}</div>
    //       <div>primer nombre: {data.firstName}</div>
    //       <div>segundo nombre: {data.secondName} </div>
    //       <div>primer apellido: {data.firstLastName} </div>
    //       <div>segundo apellido: {data.secondLastName} </div>
    //       <div>correo electrónico: {data.email} </div>
    //       <div>nombre de usuario: {data.userName} </div>
    //       )
          
    //     )
    //   }
    // </div>

      <table className="table">
        <thead>
          <tr>
            <td>Customer ID</td>
            <td>primer nombre</td>
            <td>segundo nombre</td>
            <td>primer apellido</td>
            <td>segundo apellido</td>
            <td>correo electrónico</td>
            <td>nombre de usuario</td>
          </tr>
        </thead>
        <tbody>
          {userdata.map(
            (data) => (
              <tr key={data.id}>
                <td>{data.id}</td>
                <td>{data.firstName}</td>
                <td>{data.secondName}</td>
                <td>{data.firstLastName}</td>
                <td>{data.secondLastName}</td>
                <td>{data.email}</td>
                <td>{data.userName}</td>
              </tr>
            )
          )
          }
        </tbody>
      </table>
    );
  }
}

export default DataUser;
// import React from 'react';
// import useToken from '../../useToken';

// async function getData(token) {
//   return fetch('http://localhost:3001/personalData', {
//     method: 'GET',
//     headers: {
//       'Content-Type': 'application/json',
//       'access-token': token
//     }
//   })
//     // .then(data => data.json())
//     .then(data => {
//       if (!data.ok) throw new Error('Response is NOT ok')
//       return data.json()
//     })
//     .then((dataResponse)=>{
//       console.log("Respuesta", dataResponse)
//     })
//     .catch(console.log)
// }

// export default function DataUser() {

//   getData(useToken().token);
//   // console.log("desde")
//   return (

//     <div className="book-details">
//       <div>Datos personales </div>
//       {/* <div>ID customer: {data.id}</div>
//       <div>primer nombre: {data.firstName}</div>
//       <div>segundo nombre: {data.secondName} </div>
//       <div>primer apellido: {data.firstLastName} </div>
//       <div>segundo apellido: {data.secondLastName} </div>
//       <div>correo electrónico: {data.email} </div>
//       <div>nombre de usuario: {data.userName} </div> */}
//     </div>

//   );
// }
