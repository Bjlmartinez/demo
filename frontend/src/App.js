import React from 'react';
// import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { BrowserRouter, Route } from 'react-router-dom';
import './App.css';
import DataUser from './components/DataUser/DataUser';
import Login from './components/Login/Login';
import useToken from './useToken';

function App() {

  const { token, setToken } = useToken();
  if (!token) {
    return <Login setToken={setToken} />
  }

  return (
    <div className="wrapper">
      <h1>Información de usuario</h1>
      {/* <BrowserRouter>
        <Switch>
          <Route path="/DataUser">
            <DataUser />
          </Route>
        </Switch>
      </BrowserRouter> */}
      <BrowserRouter>
      <Route exact path="/" component={DataUser}></Route>
      <Route exact path="/DataUser" component={DataUser}></Route>
      </BrowserRouter>
    </div>
  );
}

export default App;