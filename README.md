# README #

This repository content the result about the test

### In the next text describe the test ###

Se debe desarrollar en un contenedor el Front y el back de las HU1 y HU2 descritas a continuación y subirlo a la nube que desee (AWS, AZURE), compartir el código en un repositorio público de bitbucket y enviar el link del espacio desarrollado al correo adjunto en este mail. Recuerde el uso de micro front-ends, Docker y Java Springboot(cualquier lenguaje que maneje y se adapte a la necesidad)



### Consulta de información personal ###
CÓDIGO HU1: 001
NOMBRE HU: Consulta Info
DESCRIPCIÓN HU: Yo como cliente de Scotiabank-Colpatria quiero acceder a mi información personal utilizando mi ID personal de customer de manera segura para utilizar los servicios a los que tengo acceso como cliente.

Criterio de Aceptación:
* Dado que abrí la aplicación, consumir un servicio donde puede obtener y confirmar mis datos personales.
* Dado que ingresé mi customer ID, espero que el servicio muestre toda mi data personal.
* Dado una combinación de customer ID erróneo, cuando intente acceder al aplicativo, se mostrará un mensaje de error.
 

### Login de usuarios ###
CÓDIGO HU1: 002
NOMBRE HU: Login
DESCRIPCIÓN HU: Yo como cliente de Scotiabank-Colpatria quiero acceder al portal web utilizando mis credenciales personales customer, de manera segura para utilizar los servicios a los que tengo acceso como cliente.

Criterio de Aceptación:
* Se debe hacer la comprobación de credenciales en base username como lastname y al password como ID.
* Dado que intente hacer login, la comprobación debe hacerse consumiendo un servicio donde puede obtener y confirmar mis datos personales.
* Dado una combinación de credenciales erróneo, cuando intente acceder al aplicativo, se mostrará un mensaje de error.
* Version 0.0.1


### Quick summary###
The test is develop in two parts (Frontend and Backend), as define de test. It's necesary use the username (pedro) and password(pedro123.) to login in the screen to login. When the credentials it's wrong the app set message about this, but when you use the credendials, you can check the personal information.