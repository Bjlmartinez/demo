const cors = require('cors')
const express = require('express'),
      bodyParser = require('body-parser'),
      jwt = require('jsonwebtoken'),
      app = express(); 

app.use(cors());
      
const config = {
	keyValue : "secretKey741.*"
};

// 1
app.set('keyValue', config.keyValue);

// 2
app.use(bodyParser.urlencoded({ extended: true }));

// 3
app.use(bodyParser.json());

app.listen(3001,()=>{
    console.log('Servidor iniciado en el puerto 3001') 
});

// 4
app.get('/', function(req, res) {
    res.json({ message: 'recurso de entrada' });
});

// 5
app.post('/login', (req, res) => {
    if(req.body.userName === "pedro" && req.body.password === "123.") {
		const payload = {
			check:  true
		};
		const token = jwt.sign(payload, app.get('keyValue'), {
			expiresIn: 1440
		});
		res.json({
			message: 'Autenticación correcta',
			token: token
		});
    } else {
        res.json({ 
            message: "Usuario o contraseña incorrectos",
            token: null
        })
    }
})

// 6
const rutasProtegidas = express.Router(); 
rutasProtegidas.use((req, res, next) => {
    const token = req.headers['access-token'];
	
    if (token) {
      jwt.verify(token, app.get('keyValue'), (err, decoded) => {      
        if (err) {
          return res.json({ mensaje: 'Token inválida' });    
        } else {
          req.decoded = decoded;    
          next();
        }
      });
    } else {
      res.send({ 
          mensaje: 'Token no recibida.' 
      });
    }
 });

app.get('/personalData', rutasProtegidas, (req, res) => {
	const data = [
		{ id: 123456, firstName: "pedro", secondName: "jose", firstLastName: "Perez", secondLastName: "Rojas", email: "pedroperez@mail.com", userName:"pedroPerez123", password : "pass123." }
	];
	
	res.json(data);
});
